module Main where

import qualified NoFinals
import qualified Finals
-- If you use the FBrackety implementation, it can "segfault":
import qualified NfBrackety as Brackety
import qualified NfWrapped as Wrapped
import qualified NfWeak

noFinals :: IO ()
noFinals = do
    ctx <- NoFinals.contextNew "hi"
    thing <- NoFinals.thingNew ctx
    NoFinals.thingFree thing
    NoFinals.contextFree ctx

-- NOTE: This does not guarantee the order of free calls, and thus can
-- "segfault":
finals :: IO ()
finals = do
    ctx <- Finals.contextNew "hi"
    thing <- Finals.thingNew ctx
    return ()

brackety :: IO ()
brackety =
    Brackety.withContext "hi"
    $ \ctx -> Brackety.withThing ctx
    $ \thing -> return ()

wrapped :: IO ()
wrapped = Wrapped.withContext "hi" $ \ctx -> do
    t <- Wrapped.wThing ctx
    return ()

-- NOTE: The finalisers don't seem to run:
nfWeak :: IO ()
nfWeak = do
    ctx <- NfWeak.wkContext "hi"
    t <- NfWeak.wkThing ctx
    return ()

main :: IO ()
main = do
    -- noFinals
    -- brackety
    wrapped
    -- mapM_ id $ replicate 1000 nfWeak
    -- finals
