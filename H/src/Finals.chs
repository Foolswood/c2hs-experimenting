#include "ffix.h"

{# context lib="libffix" prefix="ffix" #}

module Finals (Context(..), contextNew, Thing(..), thingNew) where

{# pointer *Context foreign finalizer context_free newtype #}
{# fun context_new as ^ {`String'} -> `Context' #}

{# pointer *Thing foreign finalizer thing_free newtype #}
{# fun thing_new as ^ {`Context'} -> `Thing' #}
