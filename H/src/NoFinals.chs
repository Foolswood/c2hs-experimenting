{-# LANGUAGE StandaloneDeriving #-}
#include "ffix.h"

{# context lib="libffix" prefix="ffix" #}

module NoFinals
  ( Context, contextNew, contextFree, Thing, thingNew, thingFree
  , contextualThingFree
  ) where

{# pointer *Context newtype #}
{# fun context_new as ^ {`String'} -> `Context' #}
{# fun context_free as ^ {`Context'} -> `()' #}

{# pointer *Thing newtype #}
deriving instance Eq Thing
deriving instance Ord Thing
{# fun thing_new as ^ {`Context'} -> `Thing' #}
{# fun thing_free as ^ {`Thing'} -> `()' #}
{# fun contextual_thing_free as ^ {`Context', `Thing'} -> `()' #}
