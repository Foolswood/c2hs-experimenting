{-# LANGUAGE RankNTypes #-}

module NfWrapped (WContext, withContext, WThing, wThing, wThingDestroy) where

import Data.Set (Set)
import qualified Data.Set as Set
import Control.Concurrent.MVar
import Control.Exception (bracket)
import NoFinals

data WContext p = WContext Context (MVar (Set (WThing p)))

withContext :: String -> (WContext p -> IO a) -> IO a
withContext s = bracket newCtx freeCtx
  where
    newCtx = WContext <$> contextNew s <*> newMVar mempty
    freeCtx (WContext ctx things) = do
        allThings <- takeMVar things
        mapM_ wThingDestroy' $ Set.toList allThings
        contextFree ctx

data WThing p = WThing Thing (WContext p)

instance Eq (WThing p) where
    (WThing ta _) == (WThing tb _) = ta == tb

instance Ord (WThing p) where
    (WThing ta _) `compare` (WThing tb _) = ta `compare` tb

wThing :: WContext p -> IO (WThing p)
wThing wc@(WContext c things) = do
    existingThings <- takeMVar things
    t <- thingNew c
    let wt = WThing t wc
    putMVar things $ Set.insert wt existingThings
    return wt

wThingDestroy :: WThing p -> IO ()
wThingDestroy wt@(WThing t (WContext _ things)) = do
    allThings <- takeMVar things
    putMVar things $ Set.delete wt allThings
    wThingDestroy' wt

wThingDestroy' :: WThing p -> IO ()
wThingDestroy' (WThing t (WContext c _)) = contextualThingFree c t
