module NfWeak (wkContext, WkThing, wkThing) where

import System.Mem.Weak (addFinalizer)
import NoFinals

wkContext :: String -> IO Context
wkContext s = do
    c <- contextNew s
    addFinalizer c $ contextFree c
    return c

data WkThing = WkThing Context Thing

wkThing :: Context -> IO WkThing
wkThing c = do
    t <- thingNew c
    let wt = WkThing c t
    addFinalizer wt (thingFree t)
    return wt
