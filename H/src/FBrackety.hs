module FBrackety (withContext, withThing) where

import Foreign.Ptr (Ptr)
import Foreign.ForeignPtr (withForeignPtr, newForeignPtr_)
import Finals

withContext :: String -> (Ptr Context -> IO a) -> IO a
withContext s act = do
    Context fp <- contextNew s
    withForeignPtr fp act

withThing :: Ptr Context -> (Ptr Thing -> IO a) -> IO a
withThing cp act = do
    cfp <- newForeignPtr_ cp
    Thing fp <- thingNew $ Context cfp
    withForeignPtr fp act
