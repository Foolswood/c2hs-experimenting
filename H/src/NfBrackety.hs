module NfBrackety (withContext, withThing, contextualWithThing) where

import Control.Exception (bracket)
import NoFinals

withContext :: String -> (Context -> IO a) -> IO a
withContext s = bracket (contextNew s) contextFree

withThing :: Context -> (Thing -> IO a) -> IO a
withThing c = bracket (thingNew c) thingFree

contextualWithThing :: Context -> (Thing -> IO a) -> IO a
contextualWithThing c = bracket (thingNew c) (contextualThingFree c)
