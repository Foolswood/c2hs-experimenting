You will need:
- stack (to build the Haskell)
- meson (to build the C)

Then you can run `./initialise.sh` to do initialise stuff.
Subsequent builds can be done via `./build.sh`
