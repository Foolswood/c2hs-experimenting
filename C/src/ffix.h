#pragma once

typedef struct ffix_Context {
    int child_count;
    char * name;
} ffix_Context;

typedef struct ffix_Thing {
    ffix_Context * context;
    int child_no;
} ffix_Thing;

ffix_Context * ffix_context_new(char const * const str);
void ffix_context_free(ffix_Context * context);

ffix_Thing * ffix_thing_new(ffix_Context * context);
void ffix_thing_free(ffix_Thing * thing);
void ffix_contextual_thing_free(ffix_Context * context, ffix_Thing * thing);
