#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include "ffix.h"

ffix_Context * ffix_context_new(char const * const str) {
    ffix_Context * r = malloc(sizeof(ffix_Context));
    r->child_count = 0;
    r->name = strdup(str);
    printf("New context %s\n", r->name);
    return r;
}

void ffix_context_free(ffix_Context * context) {
    printf("Free context %s\n", context->name);
    assert(context->child_count == 0);
    free(context->name);
    free(context);
}

ffix_Thing * ffix_thing_new(ffix_Context * context) {
    ffix_Thing * r = malloc(sizeof(ffix_Thing));
    r->context = context;
    r->child_no = context->child_count++;
    printf("New thing (%s) %i\n", context->name, r->child_no);
    return r;
}

void ffix_thing_free(ffix_Thing * thing) {
    printf("Free thing (%s) %i\n", thing->context->name, thing->child_no);
    thing->context->child_count--;
    free(thing);
}

void ffix_contextual_thing_free(ffix_Context * context, ffix_Thing * thing) {
    assert(context == thing->context);
    ffix_thing_free(thing);
}
